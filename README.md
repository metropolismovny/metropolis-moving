Metropolis Moving is a Brooklyn-based moving company that provides services in Manhattan, Queens, the Bronx, Staten Island, the Tri-State area and beyond. Call (718)-710-4520 for more information!

Address: 476 Jefferson St, Brooklyn, NY 11237
Phone: 718-710-4520
